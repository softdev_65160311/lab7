/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.usermanagermentproject;

import java.util.ArrayList;

/**
 *
 * @author adisa
 */
public class UserManagermentProject {

    public static void main(String[] args) {
        User admin = new User("admin", "Administrator", "pass@1234", 'M', 'A');
        User user1 = new User("user1", "User1", "pass@1234", 'M', 'U');
        User user2 = new User("user2", "User2", "pass@1234", 'F', 'U');
        System.out.println(admin);
        System.out.println(user1);
        System.out.println(user2);
        User[] userArr = new User[3];
        userArr[0] = admin;
        userArr[1] = user1;
        userArr[2] = user2;
        for(int i=0; i<userArr.length; i++){
            System.out.println(userArr[i]);
        }
        
        ArrayList<User> userList = new ArrayList();
        userList.add(admin);
        System.out.println(userList.get(userList.size()-1)+"list size = "+userList.size());
        userList.add(user1);
        System.out.println(userList.get(userList.size()-1)+"list size = "+userList.size());
        userList.add(user2);
        System.out.println(userList.get(userList.size()-1)+"List size = "+userList.size());
        for(int i=0; i<userList.size(); i++){
            System.out.println(userList.get(i));
        }
        
        User user4 = new User("user4", "User4", "pass@1234", 'M', 'U');
        userList.set(0, user4);
        System.out.println(userList.get(0));
        userList.remove(userList.size()-1);
        for(User u : userList){
            System.out.println(u);
        }
    }
}
